import Scene from "./scene.js"
import * as Playnewton from "../playnewton.js"
import MountainLevel from "./mountain_level.js"
import MountainIntroLevel from "./mountain_intro_level.js";
import MountainOutroLevel from "./mountain_outro_level.js";
import { MenuMapKeyboardEventToPadButton } from "../utils/keyboard_mappings.js";
import TowerLevel from "./tower_level.js";
import CloudLevel from "./cloud_level.js";
import TutorialLevel from "./tutorial_level.js";
import TrainLevel from "./train_level.js";
import CityLevel from "./city_level.js";
import CityCybertuxLevel from "./city_cybertux_level.js";
import CityIntroLevel from "./city_intro_level.js";

class Adventure {
    /**
     * @type string
     */
    name;

    /**
     * @type Playnewton.GPU_Label
     */
    label;

    /**
     * @type Playnewton.GPU_Sprite
     */
    sprite;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    selectedAnimation;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    unselectedAnimation;

    constructor(name) {
        this.name = name;
        this.label = Playnewton.GPU.HUD.CreateLabel();
        Playnewton.GPU.HUD.SetLabelFont(this.label, "bold 24px monospace");
        Playnewton.GPU.HUD.SetLabelText(this.label, name);
        Playnewton.GPU.HUD.SetLabelAlign(this.label, "right");
        Playnewton.GPU.HUD.EnableLabel(this.label);
    }

    async Init() {
    }

    /**
     * 
     * @param {boolean} selected 
     */
    Update(selected) {
        if (selected) {
            Playnewton.GPU.HUD.SetLabelText(this.label, `👉${this.name}`);
            Playnewton.GPU.SetSpriteAnimation(this.sprite, this.selectedAnimation);
        } else {
            Playnewton.GPU.HUD.SetLabelText(this.label, this.name);
            Playnewton.GPU.SetSpriteAnimation(this.sprite, this.unselectedAnimation);
        }
    }

    /**
     * 
     * @param {Scene} nextScene 
     * @returns {Scene}
     */
    build(nextScene) {
        return nextScene;
    }
};

class MountainAdventure extends Adventure {

    constructor() {
        super("Mountain");
        Playnewton.GPU.HUD.SetLabelPosition(this.label, 512, 138);
    }

    async Init() {
        let bitmap = await Playnewton.DRIVE.LoadBitmap("sprites/title.png");
        let spriteset = Playnewton.GPU.CreateSpriteset(bitmap, [
            { name: "selected0", x: 1, y: 575, w: 70, h: 127 },
            { name: "selected1", x: 1, y: 703, w: 70, h: 127 },
            { name: "unselected0", x: 1, y: 831, w: 70, h: 127 }
        ]);
        this.selectedAnimation = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "selected0", delay: 500 },
            { name: "selected1", delay: 500 }
        ]);
        this.unselectedAnimation = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "unselected0", delay: 1000 }
        ]);

        this.sprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpritePosition(this.sprite, 37, 323);
        Playnewton.GPU.SetSpriteAnimation(this.sprite, this.unselectedAnimation);
        Playnewton.GPU.EnableSprite(this.sprite);
    }

    /**
     * 
     * @param {Scene} nextScene 
     * @returns {Scene}
     */
    build(nextScene) {
        /**
         * @type Scene
         */
        let scene = new MountainOutroLevel("maps/mountain/mountain_outro.tmx", nextScene);
        for (let n = 5; n >= 1; --n) {
            let level = new MountainLevel(`Mountain ${n}-5`, `maps/mountain/mountain_${n}.tmx`, scene, nextScene);
            scene = level;
        }
        return new MountainIntroLevel("maps/mountain/mountain_intro.tmx", scene);
    }

}

class TowerAdventure extends Adventure {

    constructor() {
        super("Tower");
        Playnewton.GPU.HUD.SetLabelPosition(this.label, 512, 170);
    }

    async Init() {
        let bitmap = await Playnewton.DRIVE.LoadBitmap("sprites/title.png");
        let spriteset = Playnewton.GPU.CreateSpriteset(bitmap, [
            { name: "selected0", x: 72, y: 575, w: 70, h: 127 },
            { name: "selected1", x: 72, y: 703, w: 70, h: 127 },
            { name: "unselected0", x: 72, y: 831, w: 70, h: 127 }
        ]);
        this.selectedAnimation = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "selected0", delay: 500 },
            { name: "selected1", delay: 500 }
        ]);
        this.unselectedAnimation = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "unselected0", delay: 1000 }
        ]);

        this.sprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpritePosition(this.sprite, 192, 323);
        Playnewton.GPU.SetSpriteAnimation(this.sprite, this.unselectedAnimation);
        Playnewton.GPU.EnableSprite(this.sprite);
    }

    /**
     * 
     * @param {Scene} nextScene 
     * @returns {Scene}
     */
    build(nextScene) {
        return new TowerLevel("maps/tower/tower_1.tmx", nextScene);
    }

}

class CloudAdventure extends Adventure {
    constructor() {
        super("Cloud");
        Playnewton.GPU.HUD.SetLabelPosition(this.label, 512, 202);
    }

    async Init() {
        let bitmap = await Playnewton.DRIVE.LoadBitmap("sprites/title.png");
        let spriteset = Playnewton.GPU.CreateSpriteset(bitmap, [
            { name: "selected0", x: 143, y: 575, w: 70, h: 127 },
            { name: "selected1", x: 143, y: 703, w: 70, h: 127 },
            { name: "unselected0", x: 143, y: 831, w: 70, h: 127 }
        ]);
        this.selectedAnimation = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "selected0", delay: 500 },
            { name: "selected1", delay: 500 }
        ]);
        this.unselectedAnimation = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "unselected0", delay: 1000 }
        ]);

        this.sprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpritePosition(this.sprite, 599, 323);
        Playnewton.GPU.SetSpriteAnimation(this.sprite, this.unselectedAnimation);
        Playnewton.GPU.EnableSprite(this.sprite);
    }

    /**
     * 
     * @param {Scene} nextScene 
     * @returns {Scene}
     */
    build(nextScene) {
        return new CloudLevel(nextScene);
    }
}

class TrainAdventure extends Adventure {
    constructor() {
        super("Train");
        Playnewton.GPU.HUD.SetLabelPosition(this.label, 512, 234);
    }

    async Init() {
        let bitmap = await Playnewton.DRIVE.LoadBitmap("sprites/title.png");
        let spriteset = Playnewton.GPU.CreateSpriteset(bitmap, [
            { name: "selected0", x: 214, y: 575, w: 70, h: 127 },
            { name: "selected1", x: 214, y: 703, w: 70, h: 127 },
            { name: "unselected0", x: 214, y: 831, w: 70, h: 127 }
        ]);
        this.selectedAnimation = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "selected0", delay: 500 },
            { name: "selected1", delay: 500 }
        ]);
        this.unselectedAnimation = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "unselected0", delay: 1000 }
        ]);

        this.sprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpritePosition(this.sprite, 756, 323);
        Playnewton.GPU.SetSpriteAnimation(this.sprite, this.unselectedAnimation);
        Playnewton.GPU.EnableSprite(this.sprite);
    }

    /**
     * 
     * @param {Scene} nextScene 
     * @returns {Scene}
     */
    build(nextScene) {
        return new TrainLevel(nextScene);
    }
}

class CityAdventure extends Adventure {
    constructor() {
        super("City");
        Playnewton.GPU.HUD.SetLabelPosition(this.label, 512, 266);
    }

    async Init() {
        let bitmap = await Playnewton.DRIVE.LoadBitmap("sprites/title.png");
        let spriteset = Playnewton.GPU.CreateSpriteset(bitmap, [
            { name: "selected0", x: 356, y: 575, w: 70, h: 127 },
            { name: "selected1", x: 356, y: 703, w: 70, h: 127 },
            { name: "unselected0", x: 356, y: 831, w: 70, h: 127 }
        ]);
        this.selectedAnimation = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "selected0", delay: 500 },
            { name: "selected1", delay: 500 }
        ]);
        this.unselectedAnimation = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "unselected0", delay: 1000 }
        ]);

        this.sprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpritePosition(this.sprite, 400, 323);
        Playnewton.GPU.SetSpriteAnimation(this.sprite, this.unselectedAnimation);
        Playnewton.GPU.EnableSprite(this.sprite);
    }

    /**
     * 
     * @param {Scene} nextScene 
     * @returns {Scene}
     */
    build(nextScene) {
        let cybertux = new CityCybertuxLevel("maps/city/cybertux.tmx", nextScene);
        let level = new CityLevel("maps/city/city1.tmx", cybertux);
        let intro = new CityIntroLevel("maps/city/sara_home.tmx", level);
        return intro;
    }
}

class TutorialAdventure extends Adventure {
    constructor() {
        super("Tutorial");
        Playnewton.GPU.HUD.SetLabelPosition(this.label, 512, 298);
    }

    async Init() {
        let bitmap = await Playnewton.DRIVE.LoadBitmap("sprites/title.png");
        let spriteset = Playnewton.GPU.CreateSpriteset(bitmap, [
            { name: "selected0", x: 285, y: 575, w: 70, h: 127 },
            { name: "selected1", x: 285, y: 703, w: 70, h: 127 },
            { name: "unselected0", x: 285, y: 831, w: 70, h: 127 }
        ]);
        this.selectedAnimation = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "selected0", delay: 500 },
            { name: "selected1", delay: 500 }
        ]);
        this.unselectedAnimation = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "unselected0", delay: 1000 }
        ]);

        this.sprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpritePosition(this.sprite, 912, 323);
        Playnewton.GPU.SetSpriteAnimation(this.sprite, this.unselectedAnimation);
        Playnewton.GPU.EnableSprite(this.sprite);
    }

    /**
     * 
     * @param {Scene} nextScene 
     * @returns {Scene}
     */
    build(nextScene) {
        return new TutorialLevel(nextScene);
    }
}

export default class Title extends Scene {

    /**
     * @type Array<Adventure>
     */
    adventures;

    /**
     * @type number
     */
    adventureIndex;

    constructor() {
        super();
        this.pausable = false;
    }

    async InitTitle() {
        let titleBitmap = await Playnewton.DRIVE.LoadBitmap("sprites/title.png");

        let titleBackgroundSprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpritePicture(titleBackgroundSprite, Playnewton.GPU.CreatePicture(titleBitmap, 0, 0, 1024, 576));
        Playnewton.GPU.SetSpritePosition(titleBackgroundSprite, 0, 0);
        Playnewton.GPU.EnableSprite(titleBackgroundSprite);

        let titleSprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpritePicture(titleSprite, Playnewton.GPU.CreatePicture(titleBitmap, 1, 959, 576, 65));
        Playnewton.GPU.SetSpritePosition(titleSprite, 216, 32);
        Playnewton.GPU.EnableSprite(titleSprite);
    }

    InitHUD() {
        let startLabel = Playnewton.GPU.HUD.CreateLabel();
        Playnewton.GPU.HUD.SetLabelPosition(startLabel, 1024, 564);
        Playnewton.GPU.HUD.SetLabelText(startLabel, "Press ⌨️enter or 🎮start");
        Playnewton.GPU.HUD.SetLabelFont(startLabel, "bold 12px monospace");
        Playnewton.GPU.HUD.SetLabelColor(startLabel, "#eeeeee");
        Playnewton.GPU.HUD.SetLabelAlign(startLabel, "right");
        Playnewton.GPU.HUD.EnableLabel(startLabel);

        Playnewton.GPU.EnableHUD(true);
    }

    async Start() {
        await super.Start();

        Playnewton.CTRL.MapKeyboardEventToPadButton = MenuMapKeyboardEventToPadButton;

        this.nextScene = this;
        for (let z = 0; z < 1; ++z) {
            let layer = Playnewton.GPU.GetLayer(z);
            Playnewton.GPU.EnableLayer(layer);
        }

        let musicPromise = Playnewton.APU.PlayMusic("musics/centurion_of_war/push_ahead.ogg");
        this.progress = 0;

        await this.InitTitle();
        this.progress = 30;

        this.adventures = [];
        this.adventures.push(new MountainAdventure());
        this.adventures.push(new TowerAdventure());
        this.adventures.push(new CloudAdventure());
        this.adventures.push(new TrainAdventure());
        this.adventures.push(new CityAdventure());
        this.adventures.push(new TutorialAdventure());
        this.adventureIndex = 0;
        for (let adventure of this.adventures) {
            await adventure.Init();
        }
        this.progress = 50;

        this.InitHUD();
        this.progress = 80;

        await musicPromise;
        this.progress = 100;
    }

    Stop() {
        super.Stop();

        this.adventures = [];
    }

    UpdateBodies() {
    }

    UpdateSprites() {
        let pad = Playnewton.CTRL.GetMasterPad();
        if ((pad.TestAAndResetIfPressed() || pad.TestStartAndResetIfPressed()) && this.nextScene === this) {
            Playnewton.APU.PlaySound("sounds/pause_resume.wav");
            let selectedAdventure = this.adventures[this.adventureIndex];
            this.Stop();
            this.nextScene = selectedAdventure.build(this);
            this.nextScene.Start();
        }

        if (pad.TestUpAndResetIfPressed()) {
            --this.adventureIndex;
            Playnewton.APU.PlaySound("sounds/menu-select.wav");
        }
        if (pad.TestDownAndResetIfPressed()) {
            ++this.adventureIndex;
            pad.downWasNotPressed = false;
            Playnewton.APU.PlaySound("sounds/menu-select.wav");
        }
        this.adventureIndex = Playnewton.FPU.wrap(0, this.adventureIndex, this.adventures.length - 1);
        this.adventures.forEach((adventure, index) => {
            adventure.Update(index === this.adventureIndex);
        });
    }
}
