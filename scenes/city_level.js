import Scene from "./scene.js"
import * as Playnewton from "../playnewton.js"
import Sara from "../entities/sara.js"
import Z_ORDER from "../utils/z_order.js"
import Fadeout from "../entities/fadeout.js"
import { IngameMapKeyboardEventToPadButton } from "../utils/keyboard_mappings.js"
import Announcement from "../entities/announcement.js"
import CityTraffic from "../entities/city_traffic.js"
import Sudo from "../entities/sudo.js"
import Door from "../entities/door.js"
import { MovingPlatform, MovingPlatformCollection } from "../entities/moving_platform.js";
import { DisappearingPlatform, DisappearingPlatformCollection, DisappearingPlatformDescription } from "../entities/disappearing_platform.js";


const NB_SCREEN_X = 4;
const NB_SCREEN_Y = 2;

/**
 * Enum for level state
 * @readonly
 * @enum {number}
 */
const CityLevelState = {
    START: 1,
    PLAY: 2,
    END: 7,
    GAME_OVER: 8,
    STOPPED: 9
};

export default class CityLevel extends Scene {

    /**
     * @type CityLevelState
     */
    state;

    /**
     * @type Sara
     */
    sara;

    /**
     * @type Playnewton.GPU_Bar
     */
    saraHealthBar;

    /**
     * @type string
     */
    mapPath;

    /**
     * @type Scene
     */
    nextSceneOnExit;

    /**
     * @type Fadeout
     */
    fadeout;

    /**
     * @type Playnewton.GPU_Label
     */
    skipLabel;

    /**
     * @type Announcement
     */
    levelAnnouncement;

    /**
     * @type CityTraffic
     */
    cityTraffic;

    /**
     * @type Sudo
     */
    sudo;

    /**
     * @type Door
     */
    door;

    /**
     * @type MovingPlatformCollection
     */
    movingPlatformCollection;

    /**
      * @type DisappearingPlatformCollection
      */
    disappearingPlatformCollection;

    /**
     * 
     * @param {string} mapPath 
     * @param {Scene} nextSceneOnExit 
     */
    constructor(mapPath, nextSceneOnExit) {
        super();
        this.mapPath = mapPath;
        this.nextSceneOnExit = nextSceneOnExit;
        this.pausable = false;
    }

    async InitMap() {
        let map = await Playnewton.DRIVE.LoadTmxMap(this.mapPath);

        Playnewton.PPU.SetWorldBounds(0, 0, Playnewton.GPU.screenWidth * NB_SCREEN_X,  Playnewton.GPU.screenHeight * NB_SCREEN_Y - 32);
        Playnewton.PPU.SetWorldGravity(0, 1);

        Playnewton.DRIVE.ConvertTmxMapToGPUSprites(Playnewton.GPU, map, 0, 0, Z_ORDER.BACKGROUND);
        Playnewton.DRIVE.ConvertTmxMapToPPUBodies(Playnewton.PPU, map, 0, 0);

        await this.InitMapObjects(map);
    }

    /**
     * 
     * @param {Playnewton.TMX_Map} map 
     */
    async InitMapObjects(map) {
        Playnewton.DRIVE.ForeachTmxMapObject(
            (object, objectgroup, x, y) => {
                switch (object.type) {
                    case "sara":
                        if (!this.sara) {
                            this.sara = new Sara(x, y);
                            this.sara.TeleportIn();
                        }
                        break;
                    case "sudo":
                        this.sudo.AddSudoLetter(object.tile.properties.get("letter"), x, y - 32);
                        break;
                    case "root":
                        this.sudo.AddRootLetter(object.tile.properties.get("letter"), x, y - 32);
                        break;
                    case "root_platform":
                        this.sudo.SetRootPlatform(x, y, object.width, object.height);
                        break;
                    case "moving-platform":
                        this.movingPlatformCollection.AddMovingPlatform(object.polygonPoints || object.polylinePoints);
                        break;
                    case "disappearing-platform":
                        let desc = new DisappearingPlatformDescription();
                        desc.activeDuration = parseInt(object.properties.get("active"), 10) || 3000;
                        desc.inactiveDuration = parseInt(object.properties.get("inactive"), 10) || 2000;
                        desc.initialDuration = parseInt(object.properties.get("initial"), 10) || 1000;
                        this.disappearingPlatformCollection.AddDisappearingPlatform(x, y-16, desc);
                        break;
                    case "door":
                        if(!this.door) {
                            this.door = new Door(x, y - 64);
                        }
                        break;
                    default:
                        break;
                }
            },
            map);
    }


    InitHUD() {
        this.saraHealthBar = Playnewton.GPU.HUD.CreateBar();
        Playnewton.GPU.HUD.SetBarPosition(this.saraHealthBar, 10, 10);
        Playnewton.GPU.HUD.SetBarSize(this.saraHealthBar, this.sara.maxHealth);
        Playnewton.GPU.HUD.SetBarLevel(this.saraHealthBar, this.sara.health);
        Playnewton.GPU.HUD.EnableBar(this.saraHealthBar);

        Playnewton.GPU.EnableHUD(true);
    }

    async Start() {
        this.pausable = false;
        this.state = CityLevelState.START;

        this.fadeout = null;

        await super.Start();

        Playnewton.CTRL.MapKeyboardEventToPadButton = IngameMapKeyboardEventToPadButton;
        let audioPromise = Promise.allSettled([
            Playnewton.APU.PlayMusic("musics/centurion_of_war/no_starsmixered.ogg"),
            Playnewton.APU.Preload("sounds/jump-sara.wav"),
            Playnewton.APU.Preload("sounds/hurt-sara.wav")
        ]);
        this.progress = 0;

        for (let z = Z_ORDER.MIN; z <= Z_ORDER.MAX; ++z) {
            let layer = Playnewton.GPU.GetLayer(z);
            Playnewton.GPU.EnableLayer(layer);
        }
        this.progress = 10;

        await Sara.Load();
        this.progress = 20;

        await Sudo.Load();
        this.sudo = new Sudo();
        this.progress = 30;

        await MovingPlatform.Load();
        this.movingPlatformCollection = new MovingPlatformCollection();
        this.progress = 40;

        await DisappearingPlatform.Load();
        this.disappearingPlatformCollection = new DisappearingPlatformCollection();
        this.progress = 50;

        await Door.Load();
        this.progress = 60;

        await this.InitMap();
        this.progress = 70;

        await CityTraffic.Load();
        this.cityTraffic = new CityTraffic();
        this.progress = 80;

        this.InitHUD();
        this.progress = 90;

        await audioPromise;
        this.progress = 100;
    }

    Stop() {
        super.Stop();
        Sara.Unload();
        this.sara = null;

        CityTraffic.Unload();
        this.cityTraffic = null;

        MovingPlatform.Unload();
        this.movingPlatformCollection = null;

        DisappearingPlatform.Unload();
        this.disappearingPlatformCollection = null;

        Sudo.Unload();
        this.sudo = null;

        Door.Unload();
        this.door = null;
    }

    UpdateBodies() {

        let pad = Playnewton.CTRL.GetMasterPad();
        switch (this.state) {
            case CityLevelState.START:
                this.levelAnnouncement = new Announcement("2042 : City");
                this.levelAnnouncement.Start();
                this.sara.StopWaiting();
                this.state = CityLevelState.PLAY;
                break;
            case CityLevelState.PLAY:
                this.handleSaraDeath();
                this.sudo.UpdateBody(this.sara);
                if(this.sudo.green) {
                    this.door.Open();
                }
                if(this.door.Pursue(this.sara)) {
                    this.fadeoutToNextScene();
                    this.state = CityLevelState.END;
                }
                break;
            case CityLevelState.END:
                break;
            case CityLevelState.GAME_OVER:
                if (pad.TestStartAndResetIfPressed()) {
                    Playnewton.APU.PlaySound("sounds/pause_resume.wav");
                    this.state = CityLevelState.STOPPED;
                    this.Stop();
                    this.nextScene = this.nextSceneOnExit;
                    this.nextSceneOnExit.Start();
                }
                return;
            case CityLevelState.STOPPED:
                return;
        }
        this.movingPlatformCollection.TransportSara(this.sara);
        this.movingPlatformCollection.UpdateBodies();
        this.disappearingPlatformCollection.UpdateBodies();
        this.sara.UpdateBody();
    }

    handleSaraDeath() {
        if (this.sara.dead) {
            this.state = CityLevelState.GAME_OVER;
            let layers = [];
            for (let i = Z_ORDER.MIN; i <= Z_ORDER.MAX; ++i) {
                if (i !== Z_ORDER.SARA) {
                    layers.push(i);
                }
            }
            this.fadeout = new Fadeout(1000, layers, () => {
                Playnewton.APU.PlayMusic("musics/centurion_of_war/chippy_melodyv2.ogg");

                this.pausable = false;
                let gameoverLabel = Playnewton.GPU.HUD.CreateLabel();
                Playnewton.GPU.HUD.SetLabelFont(gameoverLabel, "bold 48px monospace");
                Playnewton.GPU.HUD.SetLabelColor(gameoverLabel, "#ff0000");
                Playnewton.GPU.HUD.SetLabelAlign(gameoverLabel, "center");
                Playnewton.GPU.HUD.SetLabelPosition(gameoverLabel, 512, 288);
                Playnewton.GPU.HUD.SetLabelText(gameoverLabel, "Game over");
                Playnewton.GPU.HUD.EnableLabel(gameoverLabel);

                Playnewton.GPU.HUD.DisableBar(this.saraHealthBar);

                let continueLabel = Playnewton.GPU.HUD.CreateLabel();
                Playnewton.GPU.HUD.SetLabelFont(continueLabel, "bold 12px monospace");
                Playnewton.GPU.HUD.SetLabelAlign(continueLabel, "right");
                Playnewton.GPU.HUD.SetLabelPosition(continueLabel, 1024, 564);
                Playnewton.GPU.HUD.SetLabelColor(continueLabel, "#eeeeee");
                Playnewton.GPU.HUD.SetLabelText(continueLabel, "Press ⌨️enter or 🎮start");
                Playnewton.GPU.HUD.EnableLabel(continueLabel);
            });
        }
    }

    fadeoutToNextScene() {
        if (!this.fadeout) {
            let layers = [];
            for (let i = Z_ORDER.MIN; i <= Z_ORDER.MAX; ++i) {
                layers.push(i);
            }
            this.fadeout = new Fadeout(1000, layers, () => {
                this.state = CityLevelState.STOPPED;
                this.Stop();
                this.nextScene = this.nextSceneOnExit;
                this.nextSceneOnExit.Start();
            });
        }
    }

    UpdateSprites() {
        if(this.state === CityLevelState.STOPPED) {
            return;
        }
        this.movingPlatformCollection.UpdateSprites();
        this.sara.UpdateSprite();
        this.cityTraffic.UpdateSprites();

        let scrollX = Playnewton.FPU.bound(-Playnewton.GPU.screenWidth * (NB_SCREEN_X - 1), -this.sara.sprite.x + 512, 0);
        let scrollY = Playnewton.FPU.bound(-Playnewton.GPU.screenHeight * (NB_SCREEN_Y - 1), -this.sara.sprite.y + 288, 0);
        Playnewton.GPU.SetScroll(scrollX, scrollY);

        Playnewton.GPU.HUD.SetBarLevel(this.saraHealthBar, this.sara.health);

        if(this.levelAnnouncement) {
            this.levelAnnouncement.Update();
        }
        if (this.fadeout) {
            this.fadeout.Update();
        }
    }
}
