# License notes

The musics in this folder are great work by [Centurion_of_war](https://opengameart.org/users/centurionofwar).

## chippy_melodyv2.ogg

License: CC0
Link: https://opengameart.org/content/chippy-melody-1


## church_combat.ogg

License: CC-BY 4.0
Link: https://opengameart.org/content/church-combat

## deviation2.ogg

License: CC0
Link: https://opengameart.org/content/distortion-prototype

## guitar_53.ogg

License: CC0
Link: https://opengameart.org/users/centurionofwar

## overdrive.ogg

License: CC0
Link: https://opengameart.org/content/overdrive

## pushing_ahead.ogg

License: CC-BY 4.0
Link: https://opengameart.org/content/pushing-ahead

## pushing_yourself2.ogg

License: CC0
Link: https://opengameart.org/content/pushing-yourself
