import * as Playnewton from "../playnewton.js"
import Z_ORDER from "../utils/z_order.js";
        
export default class MovingCloud {
    /**
     * 
     * @type Playnewton.GPU_Sprite
     */
    sprite;

    /**
     * @type Array<Playnewton.GPU_SpriteAnimation>
     */
    static animations;

    /**
     * @type number
     */
    flySpeedX = -1;

    static async Load() {
        let bitmap = await Playnewton.DRIVE.LoadBitmap("sprites/moving_clouds.png");

        MovingCloud.animations = [];

        let spriteset = Playnewton.GPU.CreateSpriteset(bitmap, [
            {name: "0", x: 1, y: 1, w: 57, h: 32},
            {name: "1", x: 1, y: 34, w: 57, h: 32},
            {name: "2", x: 59, y: 1, w: 44, h: 23},
            {name: "3", x: 59, y: 25, w: 44, h: 23},
            {name: "4", x: 1, y: 67, w: 31, h: 24},
            {name: "5", x: 1, y: 92, w: 31, h: 24},
            {name: "6", x: 33, y: 66, w: 22, h: 16},
            {name: "7", x: 33, y: 83, w: 22, h: 16}
        ]);

        for(let i=0; i<=7; i++) {
            MovingCloud.animations.push(Playnewton.GPU.CreateAnimation(spriteset, [
                {name: `${i}`, delay: 1000}
            ]));
        }    
    }

    static Unload() {
        MovingCloud.animations = null;
    }

    constructor() {
        this.sprite = Playnewton.GPU.CreateSprite();
        this.Reset(Math.floor((Math.random() * Playnewton.GPU.screenWidth)));
    }

    Reset(x) {
        let animationIndex = Math.floor((Math.random() * MovingCloud.animations.length));
        Playnewton.GPU.SetSpriteAnimation(this.sprite, MovingCloud.animations[animationIndex]);
        Playnewton.GPU.SetSpritePosition(this.sprite, x, Math.floor((Math.random() * 576)));
        Playnewton.GPU.EnableSprite(this.sprite);
        if(animationIndex % 2) {
            Playnewton.GPU.SetSpriteZ(this.sprite, Z_ORDER.SKY_CLOUD_BACKGROUND);
            this.flySpeedX = 1 + Math.floor(Math.random() * 4);
        } else {
            Playnewton.GPU.SetSpriteZ(this.sprite, Z_ORDER.SKY_CLOUD_FOREGROUND);
            this.flySpeedX = 4 + Math.floor(Math.random() * 4);
        }
    }

    Update() {
        Playnewton.GPU.SetSpritePosition(this.sprite, this.sprite.x - this.flySpeedX, this.sprite.y);
        if(this.sprite.right < 0) {
            this.Reset(Playnewton.GPU.screenWidth);
        }
    }
}