import * as Playnewton from "../playnewton.js"
import Z_ORDER from "../utils/z_order.js";
import Collectible from "./collectible.js"
import Sara from "./sara.js";

/**
 * @readonly
 * @enum {number}
 */
const DoorState = {
    CLOSED: 1,
    OPEN: 2
};

const DETECT_DISTANCE = 64;
const TAKE_DISTANCE = 16;

class DoorAnimations {
    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    closed;
    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    opening;
}

export default class Door {
    /**
     * 
     * @type Playnewton.GPU_Sprite
     */
    sprite;

    /**
     *  @type DoorState
     */
    state = DoorState.CLOSED;

    /**
     * @type DoorAnimations
     */
    static animations;


    static async Load() {

        let bitmap = await Playnewton.DRIVE.LoadBitmap("maps/city/city.png");

        let spriteset = Playnewton.GPU.CreateSpriteset(bitmap, [
            { name: "door0", x: 496, y: 258, w: 32, h: 64 },
            { name: "door1", x: 529, y: 258, w: 32, h: 64 },
            { name: "door2", x: 562, y: 258, w: 32, h: 64 },
            { name: "door3", x: 595, y: 258, w: 32, h: 64 }

        ]);

        Door.animations = new DoorAnimations();

        Door.animations.closed = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "door0", delay: 1000 },
        ]);

        Door.animations.opening = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "door0", delay: 100 },
            { name: "door1", delay: 200 },
            { name: "door2", delay: 200 },
            { name: "door3", delay: 200 }
        ]);
    }

    static Unload() {
        Door.animations = null;
    }

    /**
     * 
     * @param {number} x 
     * @param {number} y 
     */
    constructor(x, y) {
        this.sprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpriteAnimation(this.sprite, Door.animations.closed);
        Playnewton.GPU.SetSpritePosition(this.sprite, x, y);
        Playnewton.GPU.SetSpriteZ(this.sprite, Z_ORDER.DOORS);
        Playnewton.GPU.EnableSprite(this.sprite);
    }

    /**
     * @param {Sara} sara
     */
    Pursue(sara) {
        let sprite = sara.sprite;
        if (this.state == DoorState.CLOSED) {
            return false;
        }
        let dx = sprite.centerX - this.sprite.centerX;
        let dy = sprite.centerY - this.sprite.centerY;
        let distance = Math.sqrt(dx ** 2 + dy ** 2);
        if (distance < TAKE_DISTANCE) {
            return true;
        }
        if (distance < DETECT_DISTANCE) {
            Playnewton.GPU.SetSpriteAnimation(this.sprite, Door.animations.opening, Playnewton.GPU_AnimationMode.ONCE);
        }
        return false;
    }

    Open() {
        this.state = DoorState.OPEN;
    }
}