import Sara from "./sara.js";
import Vulnerable from "./vulnerable.js";

export default class Enemy extends Vulnerable {

    constructor() {
        super();
    }
    /**
     * @param {Sara} sara 
     */
    Pursue(sara) {
    }

    UpdateBody() {
    }

    UpdateSprite() {
    }

    get dead() {
        return false;
    }

    get stompable() {
        return false;
    }

    get bulletable() {
        return false;
    }

    Hurt() {
    }

    HurtByStomp() {
        this.Hurt();
    }

    HurtByBullet() {
        this.Hurt();
    }
}