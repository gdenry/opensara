import * as Playnewton from "../playnewton.js"
import Z_ORDER from "../utils/z_order.js";
import Sara from "./sara.js"

/**
 * @readonly
 * @enum {number}
 */
const SudoState = {
    RED: 1,
    GREEN: 2
};


/**
 * @readonly
 * @enum {number}
 */
const SudoLetterState = {
    RED: 1,
    GREEN: 2
};


class LetterAnimations {
    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    red;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    green;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    transparentGreen;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    orange;

    /**
     * @type Object<String, LetterAnimations>
     */
    static animations = {};

    static async Load() {

        let bitmap = await Playnewton.DRIVE.LoadBitmap("maps/city/city.png");
        let spriteset = Playnewton.GPU.CreateSpriteset(bitmap, [
            { name: "s-red1", x: 232, y: 258, w: 32, h: 32 },
            { name: "s-red2", x: 232, y: 291, w: 32, h: 32 },
            { name: "s-green1", x: 232, y: 324, w: 32, h: 32 },
            { name: "s-green2", x: 232, y: 357, w: 32, h: 32 },
            { name: "u-red1", x: 265, y: 258, w: 32, h: 32 },
            { name: "u-red2", x: 265, y: 291, w: 32, h: 32 },
            { name: "u-green1", x: 265, y: 324, w: 32, h: 32 },
            { name: "u-green2", x: 265, y: 357, w: 32, h: 32 },
            { name: "d-red1", x: 298, y: 258, w: 32, h: 32 },
            { name: "d-red2", x: 298, y: 291, w: 32, h: 32 },
            { name: "d-green1", x: 298, y: 324, w: 32, h: 32 },
            { name: "d-green2", x: 298, y: 357, w: 32, h: 32 },
            { name: "o-red1", x: 331, y: 258, w: 32, h: 32 },
            { name: "o-red2", x: 331, y: 291, w: 32, h: 32 },
            { name: "o-green1", x: 331, y: 324, w: 32, h: 32 },
            { name: "o-green2", x: 331, y: 357, w: 32, h: 32 },
            { name: "o-orange1", x: 430, y: 324, w: 32, h: 32 },
            { name: "o-orange2", x: 430, y: 357, w: 32, h: 32 },
            { name: "r-red1", x: 364, y: 258, w: 32, h: 32 },
            { name: "r-red2", x: 364, y: 291, w: 32, h: 32 },
            { name: "r-orange1", x: 364, y: 324, w: 32, h: 32 },
            { name: "r-orange2", x: 364, y: 357, w: 32, h: 32 },
            { name: "t-red1", x: 397, y: 258, w: 32, h: 32 },
            { name: "t-red2", x: 397, y: 291, w: 32, h: 32 },
            { name: "t-orange1", x: 397, y: 324, w: 32, h: 32 },
            { name: "t-orange2", x: 397, y: 357, w: 32, h: 32 },
            { name: "r-transparentGreen1", x: 463, y: 258, w: 32, h: 32 },
            { name: "r-transparentGreen2", x: 463, y: 291, w: 32, h: 32 },
            { name: "o-transparentGreen1", x: 430, y: 258, w: 32, h: 32 },
            { name: "o-transparentGreen2", x: 430, y: 291, w: 32, h: 32 },
            { name: "t-transparentGreen1", x: 463, y: 324, w: 32, h: 32 },
            { name: "t-transparentGreen2", x: 463, y: 357, w: 32, h: 32 }
        ]);

        let setupAnimation = (letters, colors) => {
            for (let letter of letters) {
                LetterAnimations.animations[letter] = LetterAnimations.animations[letter] || new LetterAnimations();
                for (let color of colors) {
                    LetterAnimations.animations[letter][color] = Playnewton.GPU.CreateAnimation(spriteset, [
                        { name: `${letter}-${color}1`, delay: 100 },
                        { name: `${letter}-${color}2`, delay: 100 },
                    ]);
                }
            }
        }

        setupAnimation("sud", ["red", "green"]);
        setupAnimation("o", ["red", "green", "orange", "transparentGreen"]);
        setupAnimation("rt", ["red", "orange", "transparentGreen"]);
    }

    static Unload() {
        LetterAnimations.animations = null;
    }
}

export class SudoLetter {
    /**
     * 
     * @type Playnewton.GPU_Sprite
     */
    sprite;

    /**
     * @type Playnewton.PPU_Body
     */
    body;

    /**
     *  @type SudoLetterState
     */
    state = SudoLetterState.RED;

    /**
     * @type String
     */
    letter;

    /**
     * 
     * @param {string} letter
     * @param {number} x
     * @param {number} y
     */
    constructor(letter, x, y) {
        this.letter = letter;
        this.sprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpriteAnimation(this.sprite, LetterAnimations.animations[letter].red);
        Playnewton.GPU.SetSpritePosition(this.sprite, x, y);
        Playnewton.GPU.SetSpriteZ(this.sprite, Z_ORDER.TRANSPARENT_DOORS);
        Playnewton.GPU.EnableSprite(this.sprite);

        this.body = Playnewton.PPU.CreateBody();
        Playnewton.PPU.SetBodyPosition(this.body, x, y);
        Playnewton.PPU.SetBodyRectangle(this.body, 0, 0, 32, 32);
        Playnewton.PPU.SetBodyImmovable(this.body, true);
        let passable = new Playnewton.PPU_BodyPassable();
        passable.fromBottomToTop = false;
        passable.fromTopToBottom = false;
        passable.fromLeftToRight = false;
        passable.fromRightToLeft = false;
        Playnewton.PPU.SetBodyPassable(this.body, passable);
        Playnewton.PPU.EnableBody(this.body);
    }

    /**
     * @param {Sara} sara 
     */
    UpdateBody(sara) {
        switch (this.state) {
            case SudoLetterState.RED:
                if (Playnewton.PPU.CheckIfBodyStompOther(sara.body, this.body)) {
                    Playnewton.GPU.SetSpriteAnimation(this.sprite, LetterAnimations.animations[this.letter].green);
                    this.state = SudoLetterState.GREEN;
                }
                break;
        }
    }
}

class RootLetter {
    /**
     * 
     * @type Playnewton.GPU_Sprite
     */
    sprite;

    /**
     * @type string
     */
    letter;

    /**
     * 
     * @param {string} letter
     * @param {number} x
     * @param {number} y
     */
    constructor(letter, x, y) {
        this.letter = letter;
        this.sprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpriteAnimation(this.sprite, LetterAnimations.animations[letter].red);
        Playnewton.GPU.SetSpritePosition(this.sprite, x, y);
        Playnewton.GPU.SetSpriteZ(this.sprite, Z_ORDER.TRANSPARENT_DOORS);
        Playnewton.GPU.EnableSprite(this.sprite);
    }
}

export default class Sudo {

    /**
     * @type SudoState
     */
    state = SudoState.RED;

    /**
     * @type Array<SudoLetter>
     */
    sudoLetters = [];

    /**
     * @type Array<RootLetter>
     */
    rootLetters = [];

    /**
     * @type Playnewton.PPU_Body
     */
    rootPlatformBody;

    get green() {
        return this.state === SudoState.GREEN;
    }

    /**
     * 
     * @param {string} letter 
     * @param {number} x 
     * @param {number} y 
     */
    AddSudoLetter(letter, x, y) {
        this.sudoLetters.push(new SudoLetter(letter, x, y));
    }

    /**
     * 
     * @param {string} letter 
     * @param {number} x 
     * @param {number} y 
     */
    AddRootLetter(letter, x, y) {
        this.rootLetters.push(new RootLetter(letter, x, y));
    }

    /**
     * 
     * @param {number} x 
     * @param {number} y 
     * @param {number} w 
     * @param {number} h 
     */
    SetRootPlatform(x, y, w, h) {
        if (!this.rootPlatformBody) {
            this.rootPlatformBody = Playnewton.PPU.CreateBody();
            Playnewton.PPU.SetBodyPosition(this.rootPlatformBody, x, y);
            Playnewton.PPU.SetBodyRectangle(this.rootPlatformBody, 0, 0, w, h);
            Playnewton.PPU.SetBodyImmovable(this.rootPlatformBody, true);
            Playnewton.PPU.EnableBody(this.rootPlatformBody);
        }
    }

    /**
     * @param {Sara} sara 
     */
    UpdateBody(sara) {
        if(this.state === SudoState.GREEN) {
            return;
        }
        let nbGreenSudo = 0;
        for (let sudoLetter of this.sudoLetters) {
            sudoLetter.UpdateBody(sara);
            if (sudoLetter.state === SudoLetterState.GREEN) {
                ++nbGreenSudo;
            }
        }
        if (nbGreenSudo >= this.rootLetters.length) {
            for (let rootLetter of this.rootLetters) {
                Playnewton.GPU.SetSpriteAnimation(rootLetter.sprite, LetterAnimations.animations[rootLetter.letter].transparentGreen);
            }
            Playnewton.PPU.DisableBody(this.rootPlatformBody);
            this.state = SudoState.GREEN;
        } else {
            for (let i = 0, n = Math.min(nbGreenSudo, this.rootLetters.length); i < n; ++i) {
                Playnewton.GPU.SetSpriteAnimation(this.rootLetters[i].sprite, LetterAnimations.animations[this.rootLetters[i].letter].orange);
            }
        }
    }

    static async Load() {
        await LetterAnimations.Load();
    }
    static async Unload() {
        await LetterAnimations.Unload();
    }
}