import * as Playnewton from "../playnewton.js"
import Z_ORDER from "../utils/z_order.js";

/**
 * @readonly
 * @enum {number}
 * @enu
 */
const CarDistance = {
    FAR: 0,
    NEAR: 1
};

/**
 * 
 * @param {CarDistance} distance 
 */
function CarDistanceToString(distance) {
    switch (distance) {
        case CarDistance.FAR:
            return "FAR";
        case CarDistance.NEAR:
            return "NEAR";
    }
}


/**
 * @readonly
 * @enum {number}
 */
const CarDirection = {
    LEFT: 0,
    RIGHT: 1
};

/**
 * 
 * @param {CarDirection} direction 
 */
function CarDirectionToString(direction) {
    switch (direction) {
        case CarDirection.LEFT:
            return "LEFT";
        case CarDirection.RIGHT:
            return "RIGHT";
    }
}


class CarModel {
    /**
     * @type {string}
     */
    name;

    /**
     * @type {CarDistance}
     */
    distance;

    /**
     * @type {CarDirection}
     */
    direction;

    /**
     * @type number
     */
    velocityX;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    animation;

    /**
     * @type number
     */
    nbFrames;
}

class CityCar {

    /**
     * @type CarModel
     */
    model;

    /**
     * 
     * @type Playnewton.GPU_Sprite
     */
    sprite;

    /**
     * 
     * @param {CarModel} model
     * @param {number} x 
     * @param {number} y 
     */
    constructor(model, x, y) {
        this.model = model;
        this.sprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpriteAnimation(this.sprite, this.model.animation);
        Playnewton.GPU.SetSpritePosition(this.sprite, x, y - this.sprite.height / 2);
        Playnewton.GPU.SetSpriteZ(this.sprite, this.model.distance === CarDistance.NEAR ? Z_ORDER.CITY_TRAFFIC_NEAR : Z_ORDER.CITY_TRAFFIC_FAR);
        Playnewton.GPU.EnableSprite(this.sprite);
    }

    /**
     * @type Array<CarModel>
     */
    static jetModels;

    /**
     * @type Array<CarModel>
     */
    static rotorModels;

    static async Load() {
        let bitmap = await Playnewton.DRIVE.LoadBitmap("maps/city/city.png");

        let spriteset = Playnewton.GPU.CreateSpriteset(bitmap, [
            { name: "BLUE-NEAR-LEFT-1", x: 1, y: 577, w: 39, h: 17 },
            { name: "BLUE-NEAR-LEFT-2", x: 41, y: 577, w: 39, h: 17 },
            { name: "BLUE-NEAR-LEFT-3", x: 81, y: 577, w: 39, h: 17 },
            { name: "BLUE-NEAR-LEFT-4", x: 121, y: 577, w: 39, h: 17 },
            { name: "BLUE-NEAR-RIGHT-1", x: 161, y: 577, w: 39, h: 17 },
            { name: "BLUE-NEAR-RIGHT-2", x: 201, y: 577, w: 39, h: 17 },
            { name: "BLUE-NEAR-RIGHT-3", x: 241, y: 577, w: 39, h: 17 },
            { name: "BLUE-NEAR-RIGHT-4", x: 281, y: 577, w: 39, h: 17 },
            { name: "BLUE-FAR-LEFT-1", x: 260, y: 648, w: 19, h: 8 },
            { name: "BLUE-FAR-RIGHT-1", x: 280, y: 648, w: 19, h: 8 },
            { name: "GRAY-NEAR-LEFT-1", x: 1, y: 595, w: 56, h: 35 },
            { name: "GRAY-NEAR-LEFT-2", x: 58, y: 595, w: 56, h: 35 },
            { name: "GRAY-NEAR-LEFT-3", x: 115, y: 595, w: 56, h: 35 },
            { name: "GRAY-NEAR-LEFT-4", x: 172, y: 595, w: 56, h: 35 },
            { name: "GRAY-NEAR-RIGHT-1", x: 229, y: 595, w: 56, h: 35 },
            { name: "GRAY-NEAR-RIGHT-2", x: 286, y: 595, w: 56, h: 35 },
            { name: "GRAY-NEAR-RIGHT-3", x: 343, y: 595, w: 56, h: 35 },
            { name: "GRAY-NEAR-RIGHT-4", x: 400, y: 595, w: 56, h: 35 },
            { name: "GRAY-FAR-LEFT-1", x: 260, y: 657, w: 28, h: 17 },
            { name: "GRAY-FAR-RIGHT-1", x: 288, y: 657, w: 28, h: 17 },
            { name: "RED-NEAR-LEFT-1", x: 1, y: 631, w: 35, h: 16 },
            { name: "RED-NEAR-LEFT-2", x: 37, y: 631, w: 35, h: 16 },
            { name: "RED-NEAR-LEFT-3", x: 73, y: 631, w: 35, h: 16 },
            { name: "RED-NEAR-LEFT-4", x: 109, y: 631, w: 35, h: 16 },
            { name: "RED-NEAR-LEFT-5", x: 145, y: 631, w: 35, h: 16 },
            { name: "RED-NEAR-RIGHT-1", x: 181, y: 631, w: 35, h: 16 },
            { name: "RED-NEAR-RIGHT-2", x: 217, y: 631, w: 35, h: 16 },
            { name: "RED-NEAR-RIGHT-3", x: 253, y: 631, w: 35, h: 16 },
            { name: "RED-NEAR-RIGHT-4", x: 289, y: 631, w: 35, h: 16 },
            { name: "RED-NEAR-RIGHT-5", x: 325, y: 631, w: 35, h: 16 },
            { name: "RED-FAR-LEFT-1", x: 260, y: 675, w: 19, h: 8 },
            { name: "RED-FAR-RIGHT-1", x: 278, y: 675, w: 19, h: 8 },
            { name: "HELICO-NEAR-LEFT-1", x: 1, y: 648, w: 128, h: 47 },
            { name: "HELICO-NEAR-LEFT-2", x: 1, y: 696, w: 128, h: 47 },
            { name: "HELICO-NEAR-LEFT-3", x: 1, y: 744, w: 128, h: 47 },
            { name: "HELICO-NEAR-LEFT-4", x: 1, y: 792, w: 128, h: 47 },
            { name: "HELICO-NEAR-RIGHT-1", x: 130, y: 648, w: 128, h: 47 },
            { name: "HELICO-NEAR-RIGHT-2", x: 130, y: 696, w: 128, h: 47 },
            { name: "HELICO-NEAR-RIGHT-3", x: 130, y: 744, w: 128, h: 47 },
            { name: "HELICO-NEAR-RIGHT-4", x: 130, y: 792, w: 128, h: 47 },
            { name: "HELICO-FAR-LEFT-1", x: 260, y: 684, w: 64, h: 23 },
            { name: "HELICO-FAR-LEFT-2", x: 260, y: 708, w: 64, h: 23 },
            { name: "HELICO-FAR-LEFT-3", x: 260, y: 732, w: 64, h: 23 },
            { name: "HELICO-FAR-LEFT-4", x: 260, y: 756, w: 64, h: 23 },
            { name: "HELICO-FAR-RIGHT-1", x: 325, y: 684, w: 64, h: 23 },
            { name: "HELICO-FAR-RIGHT-2", x: 325, y: 708, w: 64, h: 23 },
            { name: "HELICO-FAR-RIGHT-3", x: 325, y: 732, w: 64, h: 23 },
            { name: "HELICO-FAR-RIGHT-4", x: 325, y: 756, w: 64, h: 23 },
            { name: "ZEPPELIN-NEAR-LEFT-1", x: 610, y: 400, w: 206, h: 103 },
            { name: "ZEPPELIN-NEAR-LEFT-2", x: 610, y: 504, w: 206, h: 103 },
            { name: "ZEPPELIN-NEAR-LEFT-3", x: 610, y: 608, w: 206, h: 103 },
            { name: "ZEPPELIN-NEAR-LEFT-4", x: 610, y: 712, w: 206, h: 103 },
            { name: "ZEPPELIN-NEAR-LEFT-5", x: 610, y: 816, w: 206, h: 103 },
            { name: "ZEPPELIN-NEAR-LEFT-6", x: 610, y: 920, w: 206, h: 103 },
            { name: "ZEPPELIN-NEAR-RIGHT-1", x: 817, y: 400, w: 206, h: 103 },
            { name: "ZEPPELIN-NEAR-RIGHT-2", x: 817, y: 504, w: 206, h: 103 },
            { name: "ZEPPELIN-NEAR-RIGHT-3", x: 817, y: 608, w: 206, h: 103 },
            { name: "ZEPPELIN-NEAR-RIGHT-4", x: 817, y: 712, w: 206, h: 103 },
            { name: "ZEPPELIN-NEAR-RIGHT-5", x: 817, y: 816, w: 206, h: 103 },
            { name: "ZEPPELIN-NEAR-RIGHT-6", x: 817, y: 920, w: 206, h: 103 },
            { name: "ZEPPELIN-FAR-LEFT-1", x: 390, y: 631, w: 103, h: 51 },
            { name: "ZEPPELIN-FAR-LEFT-2", x: 390, y: 683, w: 103, h: 51 },
            { name: "ZEPPELIN-FAR-LEFT-3", x: 390, y: 735, w: 103, h: 51 },
            { name: "ZEPPELIN-FAR-LEFT-4", x: 390, y: 787, w: 103, h: 51 },
            { name: "ZEPPELIN-FAR-LEFT-5", x: 390, y: 839, w: 103, h: 51 },
            { name: "ZEPPELIN-FAR-LEFT-6", x: 390, y: 891, w: 103, h: 51 },
            { name: "ZEPPELIN-FAR-RIGHT-1", x: 494, y: 631, w: 103, h: 51 },
            { name: "ZEPPELIN-FAR-RIGHT-2", x: 494, y: 683, w: 103, h: 51 },
            { name: "ZEPPELIN-FAR-RIGHT-3", x: 494, y: 735, w: 103, h: 51 },
            { name: "ZEPPELIN-FAR-RIGHT-4", x: 494, y: 787, w: 103, h: 51 },
            { name: "ZEPPELIN-FAR-RIGHT-5", x: 494, y: 839, w: 103, h: 51 },
            { name: "ZEPPELIN-FAR-RIGHT-6", x: 494, y: 891, w: 103, h: 51 }
        ]);

        CityCar.jetModels = [
            { name: "BLUE", distance: CarDistance.NEAR, direction: CarDirection.LEFT, velocityX: -4, animation: null, nbFrames: 4 },
            { name: "BLUE", distance: CarDistance.NEAR, direction: CarDirection.RIGHT, velocityX: 4, animation: null, nbFrames: 4 },
            { name: "BLUE", distance: CarDistance.FAR, direction: CarDirection.LEFT, velocityX: -2, animation: null, nbFrames: 1 },
            { name: "BLUE", distance: CarDistance.FAR, direction: CarDirection.RIGHT, velocityX: 2, animation: null, nbFrames: 1 },
            { name: "GRAY", distance: CarDistance.NEAR, direction: CarDirection.LEFT, velocityX: -4, animation: null, nbFrames: 4 },
            { name: "GRAY", distance: CarDistance.NEAR, direction: CarDirection.RIGHT, velocityX: 4, animation: null, nbFrames: 4 },
            { name: "GRAY", distance: CarDistance.FAR, direction: CarDirection.LEFT, velocityX: -2, animation: null, nbFrames: 1 },
            { name: "GRAY", distance: CarDistance.FAR, direction: CarDirection.RIGHT, velocityX: 2, animation: null, nbFrames: 1 },
            { name: "RED", distance: CarDistance.NEAR, direction: CarDirection.LEFT, velocityX: -4, animation: null, nbFrames: 5 },
            { name: "RED", distance: CarDistance.NEAR, direction: CarDirection.RIGHT, velocityX: 4, animation: null, nbFrames: 5 },
            { name: "RED", distance: CarDistance.FAR, direction: CarDirection.LEFT, velocityX: -2, animation: null, nbFrames: 1 },
            { name: "RED", distance: CarDistance.FAR, direction: CarDirection.RIGHT, velocityX: 2, animation: null, nbFrames: 1 },
            { name: "HELICO", distance: CarDistance.NEAR, direction: CarDirection.LEFT, velocityX: -1, animation: null, nbFrames: 4 },
            { name: "HELICO", distance: CarDistance.NEAR, direction: CarDirection.RIGHT, velocityX: 1, animation: null, nbFrames: 4 },
            { name: "HELICO", distance: CarDistance.FAR, direction: CarDirection.LEFT, velocityX: -0.5, animation: null, nbFrames: 4 },
            { name: "HELICO", distance: CarDistance.FAR, direction: CarDirection.RIGHT, velocityX: 0.5, animation: null, nbFrames: 4 },
            { name: "ZEPPELIN", distance: CarDistance.NEAR, direction: CarDirection.LEFT, velocityX: -0.5, animation: null, nbFrames: 6 },
            { name: "ZEPPELIN", distance: CarDistance.NEAR, direction: CarDirection.RIGHT, velocityX: 0.5, animation: null, nbFrames: 6 },
            { name: "ZEPPELIN", distance: CarDistance.FAR, direction: CarDirection.LEFT, velocityX: -0.25, animation: null, nbFrames: 6 },
            { name: "ZEPPELIN", distance: CarDistance.FAR, direction: CarDirection.RIGHT, velocityX: 0.25, animation: null, nbFrames: 6 }
        ];

        for (let model of CityCar.jetModels) {
            let frameDesc = [];
            for (let frame = 1; frame <= model.nbFrames; frame++) {
                frameDesc.push({ name: `${model.name}-${CarDistanceToString(model.distance)}-${CarDirectionToString(model.direction)}-${frame}`, delay: 100 });
            }
            model.animation = Playnewton.GPU.CreateAnimation(spriteset, frameDesc);
        }
    }

    static Unload() {
        CityCar.jetModels = null;
    }

    /**
     * @param {number} laneLeft 
     * @param {number} laneRight 
     */
    UpdateSprite(laneLeft, laneRight) {
        let x = this.sprite.x + this.model.velocityX;
        if ( x <= laneLeft) {
            x = laneRight - (laneLeft - x);
        } else if (x >= laneRight) {
            x = laneLeft - (laneRight - x);
        }
        Playnewton.GPU.SetSpritePosition(this.sprite, x, this.sprite.y);
    }



}

class CityTrafficLane {

    /**
     * @type CarDistance
     */
    distance;

    /**
     * @type CarDirection
     */
    direction;

    /**
     * @type number
     */
    y;

    /**
     * @type number
     */
    left;
    
    /**
     * @type number
     */
    right;

    /**
     * @type Array<CityCar>
     */
    cars = [];

    /**
     * @param {Array<CarModel>} models
     * @param {CarDirection} direction
     * @param {number} y 
     */
    constructor(models, direction, y) {
        this.left = Playnewton.PPU.world.bounds.left - models.reduce((w, model) => Math.max(w, model.animation.maxWidth), 0);
        this.right = Playnewton.PPU.world.bounds.right + models.reduce((w, model) => Math.max(w, model.animation.maxWidth), 0);

        if(direction === CarDirection.LEFT) {
            let x = Playnewton.PPU.world.bounds.width - models[0].animation.maxWidth - 1;
            for (let model of models) {
                if(x < this.left) {
                    break;
                }
                let car = new CityCar(model, x, y);
                this.cars.push(car);
                x -= model.animation.maxWidth + 128;
            }
        } else {
            let x = 0;
            for (let model of models) {
                if((x+model.animation.maxWidth) >= this.right) {
                    break;
                }
                let car = new CityCar(model, x, y);
                this.cars.push(car);
                x += model.animation.maxWidth + 128;
            }
        }

    }

    UpdateSprites() {
        for (let car of this.cars) {
            car.UpdateSprite(this.left, this.right);
        }
    }
}

export default class CityTraffic {

    /**
     * @type Array<CityTrafficLane>
     */
    lanes = [];

    constructor() {
        this.lanes.push(new CityTrafficLane(Playnewton.RAND.pickNFiltered(CityCar.jetModels, 1, (model) => ["ZEPPELIN"].includes(model.name) && model.direction === CarDirection.RIGHT && model.distance === CarDistance.FAR), CarDirection.RIGHT, 288+10));
        this.lanes.push(new CityTrafficLane(Playnewton.RAND.pickNFiltered(CityCar.jetModels, 1, (model) => ["ZEPPELIN"].includes(model.name) && model.direction === CarDirection.LEFT && model.distance === CarDistance.FAR), CarDirection.LEFT, 288+40));
        this.lanes.push(new CityTrafficLane(Playnewton.RAND.pickNFiltered(CityCar.jetModels, 1, (model) => ["HELICO"].includes(model.name) && model.direction === CarDirection.LEFT && model.distance === CarDistance.FAR), CarDirection.LEFT, 288+80));
        this.lanes.push(new CityTrafficLane(Playnewton.RAND.pickNFiltered(CityCar.jetModels, 1, (model) => ["HELICO"].includes(model.name) && model.direction === CarDirection.RIGHT && model.distance === CarDistance.FAR), CarDirection.RIGHT, 288+100));
        this.lanes.push(new CityTrafficLane(Playnewton.RAND.pickNFiltered(CityCar.jetModels, 32, (model) => ["RED", "BLUE", "GRAY"].includes(model.name) && model.direction === CarDirection.LEFT && model.distance === CarDistance.FAR), CarDirection.RIGHT, 576+300));
        this.lanes.push(new CityTrafficLane(Playnewton.RAND.pickNFiltered(CityCar.jetModels, 32, (model) => ["RED", "BLUE", "GRAY"].includes(model.name) && model.direction === CarDirection.RIGHT && model.distance === CarDistance.FAR), CarDirection.RIGHT, 576+350));

        this.lanes.push(new CityTrafficLane(Playnewton.RAND.pickNFiltered(CityCar.jetModels, 1, (model) => ["ZEPPELIN"].includes(model.name) && model.direction === CarDirection.LEFT && model.distance === CarDistance.NEAR), CarDirection.LEFT, 288+120));
        this.lanes.push(new CityTrafficLane(Playnewton.RAND.pickNFiltered(CityCar.jetModels, 1, (model) => ["ZEPPELIN"].includes(model.name) && model.direction === CarDirection.RIGHT && model.distance === CarDistance.NEAR), CarDirection.RIGHT, 288+150));
        this.lanes.push(new CityTrafficLane(Playnewton.RAND.pickNFiltered(CityCar.jetModels, 1, (model) => ["HELICO"].includes(model.name) && model.direction === CarDirection.RIGHT && model.distance === CarDistance.NEAR), CarDirection.RIGHT, 288+180));
        this.lanes.push(new CityTrafficLane(Playnewton.RAND.pickNFiltered(CityCar.jetModels, 1, (model) => ["HELICO"].includes(model.name) && model.direction === CarDirection.LEFT && model.distance === CarDistance.NEAR), CarDirection.LEFT, 288+200));
        this.lanes.push(new CityTrafficLane(Playnewton.RAND.pickNFiltered(CityCar.jetModels, 32, (model) => ["RED", "BLUE", "GRAY"].includes(model.name) && model.direction === CarDirection.RIGHT && model.distance === CarDistance.NEAR), CarDirection.RIGHT, 576+450));
        this.lanes.push(new CityTrafficLane(Playnewton.RAND.pickNFiltered(CityCar.jetModels, 32, (model) => ["RED", "BLUE", "GRAY"].includes(model.name) && model.direction === CarDirection.LEFT && model.distance === CarDistance.NEAR), CarDirection.LEFT, 576+500));
    }

    UpdateSprites() {
        for (let lane of this.lanes) {
            lane.UpdateSprites();
        }
    }

    static async Load() {
        CityCar.Load();
    }

    static Unload() {
        CityCar.Unload();
    }
}