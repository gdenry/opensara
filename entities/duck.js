import Enemy from "./enemy.js"
import * as Playnewton from "../playnewton.js"
import Z_ORDER from "../utils/z_order.js";
import Sara from "./sara.js";
import { BulletAnimations, Gun } from "./bullet.js";

/**
 * @readonly
 * @enum {number}
 */
const DuckState = {
    FLY: 1,
    HURT: 2,
    DYING: 3,
    DEAD: 4
};

/**
 * 
 * @type DuckAnimations
 */
class DuckAnimations {
    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    fly;
    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    hurt;
    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    die;
}

export default class Duck extends Enemy {

    /**
     * 
     * @type Playnewton.GPU_Sprite
     */
    sprite;

    /**
     * 
     * @type Playnewton.PPU_Body
     */
    body;

    /**
     *  @type DuckState
     */
    state;

    /**
     * @type number
     */
    health = 2;

    get dead() {
        return this.health <= 0;
    }

    /**
     * @type Gun
     */
    gun;

    /**
     * @type number
     */
    flySpeed = 2;

    /**
     * @type number
     */
    fleeSpeed = 4;

    /**
     * @type DuckAnimations
     */
    static animations;

    /**
     * @type BulletAnimations
     */
    static bulletAnimations;

    /**
     * @type Array<Playnewton.PPU_Point>
     */
    destinations = [];

    /**
     * @type number
     */
    currentDestination = 0;

    static async Load() {
        let bitmap = await Playnewton.DRIVE.LoadBitmap("sprites/duck.png");

        let spriteset = Playnewton.GPU.CreateSpriteset(bitmap, [
            { name: "fly00", x: 1, y: 1, w: 138, h: 107 },
            { name: "fly01", x: 140, y: 1, w: 138, h: 107 },
            { name: "fly02", x: 1, y: 109, w: 138, h: 107 },
            { name: "hurt00", x: 140, y: 109, w: 138, h: 107 },
            { name: "bullet-fly00", x: 1, y: 223, w: 32, h: 32 },
            { name: "bullet-fly01", x: 34, y: 223, w: 32, h: 32 },
            { name: "bullet-fly02", x: 67, y: 223, w: 32, h: 32 },
            { name: "bullet-fly03", x: 100, y: 223, w: 32, h: 32 },
            { name: "bullet-explode00", x: 133, y: 223, w: 32, h: 32 },
            { name: "bullet-explode01", x: 166, y: 223, w: 32, h: 32 },
            { name: "bullet-explode02", x: 199, y: 223, w: 32, h: 32 },
            { name: "bullet-explode03", x: 232, y: 223, w: 32, h: 32 }
        ]);

        Duck.animations = new DuckAnimations();

        Duck.animations.fly = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "fly00", delay: 100 },
            { name: "fly01", delay: 100 },
            { name: "fly02", delay: 100 },
            { name: "fly01", delay: 100 }
        ]);

        Duck.animations.die = Duck.animations.hurt = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "fly00", delay: 100 },
            { name: "hurt00", delay: 100 },
            { name: "fly01", delay: 100 },
            { name: "hurt00", delay: 100 },
            { name: "fly02", delay: 100 },
            { name: "hurt00", delay: 100 },
            { name: "fly01", delay: 100 },
            { name: "hurt00", delay: 100 }
        ]);

        Duck.bulletAnimations = new BulletAnimations();

        Duck.bulletAnimations.grow = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "bullet-fly00", delay: 1000 }
        ]);

        Duck.bulletAnimations.fly = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "bullet-fly00", delay: 100 },
            { name: "bullet-fly01", delay: 100 },
            { name: "bullet-fly02", delay: 100 },
            { name: "bullet-fly03", delay: 100 }
        ]);

        Duck.bulletAnimations.explode = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "bullet-explode00", delay: 100 },
            { name: "bullet-explode01", delay: 100 },
            { name: "bullet-explode02", delay: 100 },
            { name: "bullet-explode03", delay: 100 }
        ]);
    }

    static Unload() {
        Duck.animations = null;
        Duck.bulletAnimations = null;
    }

    constructor(x, y) {
        super();
        this.destinations.push(new Playnewton.PPU_Point(x, y));
        this.destinations.push(new Playnewton.PPU_Point(Playnewton.PPU.world.bounds.left - 71, y));
        this.sprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpriteAnimation(this.sprite, Duck.animations.fly);
        Playnewton.GPU.SetSpriteZ(this.sprite, Z_ORDER.ENEMIES);
        Playnewton.GPU.EnableSprite(this.sprite);

        this.body = Playnewton.PPU.CreateBody();
        Playnewton.PPU.SetBodyRectangle(this.body, 0, 0, 71, 23);
        Playnewton.PPU.SetBodyPosition(this.body, x, y);
        Playnewton.PPU.SetBodyVelocityBounds(this.body, -this.flySpeed, this.flySpeed, -this.flySpeed, this.flySpeed);
        Playnewton.PPU.EnableBody(this.body);

        this.state = DuckState.FLY;
        this.gun = new Gun({animations: Duck.bulletAnimations, fireSound: "sounds/shoot-duck.wav", maxBullets: 1, maxBulletsPerShot: 1, delayBetweenTwoShotsInMs: 2000, growSpeed: 0.1});
    }

    UpdateSprite() {
        switch (this.state) {
            case DuckState.FLY:
                Playnewton.GPU.SetSpriteAnimation(this.sprite, Duck.animations.fly);
                break;
            case DuckState.HURT:
                Playnewton.GPU.SetSpriteAnimation(this.sprite, Duck.animations.hurt, Playnewton.GPU_AnimationMode.ONCE);
                if (this.sprite.animationStopped) {
                    Playnewton.GPU.SetSpriteAnimation(this.sprite, Duck.animations.fly);
                    this.state = DuckState.FLY;
                }
                break;
            case DuckState.DYING:
                Playnewton.GPU.SetSpriteAnimation(this.sprite, Duck.animations.die);
                break;
        }
        Playnewton.GPU.SetSpritePosition(this.sprite, this.body.position.x - 53, this.body.position.y - 51);
        this.gun.UpdateSprite();
    }

    UpdateBody() {
        switch (this.state) {
            case DuckState.FLY:
                let arrived = this.flyToDestination();
                if (arrived && !Playnewton.PPU.CheckIfBodyIsInWorldBound(this.body)) {
                    Playnewton.GPU.DisableSprite(this.sprite);
                    Playnewton.PPU.DisableBody(this.body);
                    this.health = 0;
                    this.state = DuckState.DEAD;
                }
                break;
            case DuckState.HURT:
                break;
            case DuckState.DYING:
                Playnewton.PPU.SetBodyVelocityBounds(this.body, -this.fleeSpeed, this.fleeSpeed, -this.fleeSpeed, this.fleeSpeed);
                Playnewton.PPU.SetBodyVelocity(this.body, -this.fleeSpeed, this.fleeSpeed);
                if (!Playnewton.PPU.CheckIfBodyIsInWorldBound(this.body)) {
                    Playnewton.GPU.DisableSprite(this.sprite);
                    Playnewton.PPU.DisableBody(this.body);
                    this.state = DuckState.DEAD;
                }
                break;
            case DuckState.DEAD:
                break;
        }
        this.gun.UpdateBody();
    }

    flyToDestination() {
        if (this.destinations.length > 0) {
            let destination = this.destinations[this.currentDestination];
            let dx = destination.x - this.body.centerX;
            let dy = destination.y - this.body.centerY;
            let distance = Math.sqrt(dx ** 2 + dy ** 2);
            if (distance < 32) {
                this.currentDestination++;
                if (this.currentDestination >= this.destinations.length) {
                    this.currentDestination = this.destinations.length - 1;
                    return true;
                }
            }
            let s = this.flySpeed / distance;
            dx *= s;
            dy *= s;
            Playnewton.PPU.SetBodyVelocity(this.body, dx, dy);
        }
        return false;
    }

    get bulletable() {
        return true;
    }

    /**
     * 
     * @param {Sara} sara 
     */
    Pursue(sara) {
        switch (this.state) {
            case DuckState.FLY:
                this.gun.fire(this.body.position.x - 53, this.body.position.y + 25, -5, 0);
                break;
        }
        this.gun.Pursue(sara);
    }

    Hurt() {
        switch (this.state) {
            case DuckState.FLY:
                Playnewton.APU.PlaySound("sounds/hurt-enemy.wav");
                this.state = DuckState.HURT;
                this.health = Math.max(this.health - 1, 0);
                if (this.dead) {
                    this.state = DuckState.DYING;
                }
                break;
        }
    }
}