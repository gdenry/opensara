import * as Playnewton from "../playnewton.js"
import Z_ORDER from "../utils/z_order.js";

export default class ExplosionArea {
    /**
     * 
     * @type Array<Playnewton.GPU_Sprite>
     */
    sprites = [];

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    animation;

    /**
     * @type Playnewton.PPU_Rectangle
     */
    area;

    /**
     * @type Playnewton.CLOCK_Timer
     */
    nextExplosionTimer;

    /**
     * @param {Playnewton.PPU_Rectangle} area 
     * @param {Playnewton.GPU_SpriteAnimation} animation 
     * @param {number} nbSprite
     * @param {number} delayBetweenExplosionInMS
     */
    constructor(area, animation, nbSprite = 4, delayBetweenExplosionInMS = 200) {
        this.area = area;
        this.animation = animation;
        this.nextExplosionTimer = new Playnewton.CLOCK_Timer(delayBetweenExplosionInMS);
        this.nextExplosionTimer.Start();
        for (let i = 0; i < nbSprite; i++) {
            this.sprites.push(Playnewton.GPU.CreateSprite());
        }
    }

    Update() {
        if(this.nextExplosionTimer.elapsed) {
            let sprite = this.sprites.find( (s) => !s.enabled);
            if(sprite) {
                Playnewton.APU.PlaySound("sounds/explosion.wav");
                Playnewton.GPU.ResetSpriteAnimation(sprite, this.animation, Playnewton.GPU_AnimationMode.ONCE);
                Playnewton.GPU.SetSpritePosition(sprite, this.area.left + Math.floor((Math.random() * this.area.w)), this.area.top + Math.floor((Math.random() * this.area.h)));
                Playnewton.GPU.EnableSprite(sprite);
                Playnewton.GPU.SetSpriteZ(sprite, Z_ORDER.EXPLOSION);
                Playnewton.GPU.EnableSprite(sprite);
                this.nextExplosionTimer.Start();
            }
        }
        for(let sprite of this.sprites) {
            if(sprite.enabled && sprite.animationStopped){
                Playnewton.GPU.DisableSprite(sprite);
            }
        }
    }
}