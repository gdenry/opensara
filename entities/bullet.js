import * as Playnewton from "../playnewton.js"
import Z_ORDER from "../utils/z_order.js";
import Enemy from "./enemy.js";
import Vulnerable from "./vulnerable.js";

/**
 * Get the color as an array of red, green, and blue values, represented as
 * decimal numbers between 0 and 1.
 *
 * @function
 * @name Color#rgb
 * @returns {Array<number>} An array containing the red, green, and blue values,
 * in that order.
 */

/**
 * @readonly
 * @enum {number}
 */
export const BulletState = {
    GROW: 1,
    FLY: 2,
    EXPLODE: 3,
    EXPLODED: 4
};

export class BulletAnimations {
    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    grow;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    fly;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    explode;
}

export class Bullet extends Enemy {
    /**
     * 
     * @type Playnewton.GPU_Sprite
     */
    sprite;

    /**
     * 
     * @type Playnewton.PPU_Body
     */
    body;

    /**
     * @type number
     */
    vx;

    /**
     * @type number
     */
    vy;

    /**
     * @type number
     */
    scale = 0;

    /**
     * @type number
     */
    growSpeed = 0.02;

    /**
     * @type BulletAnimations
     */
    animations;

    /**
     * @type string
     */
    fireSound;

    /**
     * @param {BulletAnimations} animations 
     * @param {string} fireSound
     */
    constructor(animations, fireSound = null) {
        super();
        this.animations = animations;
        this.fireSound = fireSound;

        this.sprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpriteZ(this.sprite, Z_ORDER.BULLETS);
        Playnewton.GPU.EnableSprite(this.sprite);

        this.body = Playnewton.PPU.CreateBody();
        Playnewton.PPU.SetBodyAffectedByGravity(this.body, false);
        Playnewton.PPU.SetBodyCollisionMask(this.body, 0);
        Playnewton.PPU.EnableBody(this.body);

        this.state = BulletState.EXPLODED;
    }

    get canBeFired() {
        return this.state === BulletState.EXPLODED;
    }

    /**
     * 
     * @param {number} x 
     * @param {number} y 
     * @param {number} targetX 
     * @param {number} targetY 
     * @param {number} speed 
     */
    fireAt(x, y, targetX, targetY, speed) {
        let dx = targetX - x;
        let dy = targetY - y;
        let distance = Math.sqrt(dx ** 2 + dy ** 2);
        let s = speed / distance;
        dx *= s;
        dy *= s;
        this.fire(x, y, dx, dy);
    }

    /**
     * 
     * @param {number} x 
     * @param {number} y 
     * @param {number} vx 
     * @param {number} vy 
     */
    fire(x, y, vx, vy) {
        if(this.fireSound) {
            Playnewton.APU.PlaySound(this.fireSound);
        }
        Playnewton.PPU.SetBodyPosition(this.body, x, y);
        this.vx = vx;
        this.vy = vy;
        this.state = BulletState.GROW;
        this.scale = 0;
        Playnewton.GPU.SetSpriteAnimation(this.sprite, this.animations.grow);
        Playnewton.GPU.SetSpriteVisible(this.sprite, true);
        Playnewton.GPU.SetSpriteRotozoom(this.sprite, 0);
        Playnewton.PPU.SetBodyVelocityBounds(this.body, 0, 0, 0, 0);
    }

    UpdateBody() {
        switch (this.state) {
            case BulletState.GROW:
                this.scale += this.growSpeed;
                if (this.scale >= 1) {
                    this.scale = 1;
                    this.state = BulletState.FLY;
                    Playnewton.GPU.SetSpriteRotozoom(this.sprite, 1);
                    Playnewton.PPU.SetBodyVelocityBounds(this.body, -10, 10, -10, 10);
                }
                break;
            case BulletState.FLY:
                Playnewton.PPU.SetBodyVelocity(this.body, this.vx, this.vy);
                break;
            case BulletState.EXPLODE:
                break;
            case BulletState.EXPLODED:
                break;
        }
        if (!Playnewton.PPU.CheckIfBodyIsInWorldBound(this.body)) {
            Playnewton.GPU.SetSpriteVisible(this.sprite, false);
            Playnewton.PPU.SetBodyVelocityBounds(this.body, 0, 0, 0, 0);
            this.state = BulletState.EXPLODED;
        }
    }

    UpdateSprite() {
        switch (this.state) {
            case BulletState.GROW:
                Playnewton.GPU.SetSpriteAnimation(this.sprite, this.animations.grow);
                Playnewton.GPU.SetSpriteRotozoom(this.sprite, this.scale);
                Playnewton.GPU.SetSpritePosition(this.sprite, this.body.position.x - this.sprite.width / 2, this.body.position.y - this.sprite.height / 2);
                break;
            case BulletState.FLY:
                Playnewton.GPU.SetSpriteAnimation(this.sprite, this.animations.fly);
                Playnewton.GPU.SetSpritePosition(this.sprite, this.body.position.x - this.sprite.width / 2, this.body.position.y - this.sprite.height / 2);
                break;
            case BulletState.EXPLODE:
                Playnewton.GPU.SetSpriteAnimation(this.sprite, this.animations.explode, Playnewton.GPU_AnimationMode.ONCE);
                if (this.sprite.animationStopped) {
                    Playnewton.GPU.SetSpriteVisible(this.sprite, false);
                    this.state = BulletState.EXPLODED;
                }
                Playnewton.GPU.SetSpritePosition(this.sprite, this.body.position.x - this.sprite.width / 2, this.body.position.y - this.sprite.height / 2);
                break;
            case BulletState.EXPLODED:
                break;
        }
    }

    /**
     * @param {Vulnerable} target 
     */
    Pursue(target) {
        if (this.state !== BulletState.FLY) {
            return;
        }
        if (target.bulletable && Playnewton.PPU.CheckIfBodiesIntersects(this.body, target.body)) {
            target.HurtByBullet();
            Playnewton.PPU.SetBodyVelocityBounds(this.body, 0, 0, 0, 0);
            this.state = BulletState.EXPLODE;
        }
        if(target.weak_spots) {
            for(let ws of target.weak_spots) {
                this.Pursue(ws);
            }
        }
    }

}

export class Gun {

    /**
     * @type Array<Bullet>
     */
    bullets = [];

    /**
     * @type number
     */
    maxBulletsPerShot;

    /**
     * @type number
     */
    delayBetweenTwoShotsInMs;

    nbBulletsForCurrentShot = 0;

    /**
     * @type number
     */
    nextBulletTime = 0;

    /**
     * @param {Object} gunspec
     * @param {BulletAnimations} gunspec.animations
     * @param {number} gunspec.animations
     * @param {string} gunspec.fireSound
     * @param {number} gunspec.maxBullets
     * @param {number} gunspec.maxBulletsPerShot
     * @param {number} gunspec.delayBetweenTwoShotsInMs
     * @param {number} gunspec.growSpeed
     */
    constructor({animations, fireSound, maxBullets = 10, maxBulletsPerShot = 1, delayBetweenTwoShotsInMs = 1000, growSpeed = 1}) {
        for (let i = 0; i < maxBullets; i++) {
            let bullet = new Bullet(animations, fireSound);
            bullet.growSpeed = growSpeed;
            this.bullets.push(bullet);
        }
        this.maxBulletsPerShot = maxBulletsPerShot;
        this.delayBetweenTwoShotsInMs = delayBetweenTwoShotsInMs;
    }

    fireAt(x, y, targetX, targetY, speed) {
        for (let bullet of this.bullets) {
            if (this._testAndSetBulletTime(bullet)) {
                bullet.fireAt(x, y, targetX, targetY, speed);
                break;
            }
        }
    }

    fire(x, y, vx, vy) {
        for (let bullet of this.bullets) {
            if (this._testAndSetBulletTime(bullet)) {
                bullet.fire(x, y, vx, vy);
                break;
            }
        }
    }

    UpdateBody() {
        for (let bullet of this.bullets) {
            bullet.UpdateBody();
        }
    }

    UpdateSprite() {
        for (let bullet of this.bullets) {
            bullet.UpdateSprite();
        }
    }

    /**
     * @param {Vulnerable} target
     */
    Pursue(target) {
        for (let bullet of this.bullets) {
            bullet.Pursue(target);
        }
    }

    /**
     * @param {Bullet} bullet
     * @returns {boolean}
     */
    _testAndSetBulletTime(bullet) {
        if (!bullet.canBeFired) {
            return false;
        }
        this.nbBulletsForCurrentShot;
        let now = Playnewton.CLOCK.now;
        if (now <= this.nextBulletTime) {
            return false;
        }
        if(this.nbBulletsForCurrentShot > this.maxBulletsPerShot) {
            return false;
        }
        this.nbBulletsForCurrentShot++;
        if(this.nbBulletsForCurrentShot >= this.maxBulletsPerShot) {
            this.nextBulletTime = now + this.delayBetweenTwoShotsInMs;
            this.nbBulletsForCurrentShot = 0;
        }
        return true;
    }

}
