<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.0" name="cybertux" tilewidth="32" tileheight="32" tilecount="35" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="1" x="859" y="958" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 <tile id="2" x="892" y="958" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 <tile id="3" x="925" y="958" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 <tile id="4" x="958" y="958" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 <tile id="5" x="991" y="958" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 <tile id="6" x="859" y="991" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 <tile id="7" x="892" y="991" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 <tile id="8" x="925" y="991" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 <tile id="9" x="958" y="991" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 <tile id="10" x="991" y="991" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 <tile id="11" x="760" y="958" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 <tile id="12" x="793" y="958" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 <tile id="13" x="826" y="958" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 <tile id="14" x="760" y="991" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 <tile id="15" x="793" y="991" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 <tile id="16" x="826" y="991" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 <tile id="17" x="727" y="958" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 <tile id="18" x="727" y="991" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 <tile id="20" x="859" y="925" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 <tile id="21" x="892" y="925" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
  <animation>
   <frame tileid="210" duration="500"/>
   <frame tileid="211" duration="500"/>
   <frame tileid="212" duration="500"/>
   <frame tileid="213" duration="500"/>
  </animation>
 </tile>
 <tile id="22" x="859" y="859" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 <tile id="23" x="892" y="859" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 <tile id="24" x="925" y="859" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 <tile id="25" x="958" y="859" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 <tile id="26" x="991" y="859" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 <tile id="27" x="859" y="892" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 <tile id="28" x="892" y="892" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 <tile id="29" x="925" y="892" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 <tile id="30" x="958" y="892" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 <tile id="31" x="991" y="892" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>

<tile id="32" x="760" y="925" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
<tile id="33" x="793" y="925" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
<tile id="34" x="826" y="925" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 
 <tile id="210" x="892" y="925" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 <tile id="211" x="925" y="925" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 <tile id="212" x="958" y="925" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
 <tile id="213" x="991" y="925" width="32" height="32">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
  <tile id="100" x="1" y="1" width="640" height="506" type="cybertux">
  <image width="1024" height="1024" source="cybertux.png"/>
 </tile>
</tileset>
