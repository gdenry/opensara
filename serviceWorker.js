const CACHE_NAME = 'opensara-v1';

/**
 * @type ExtendableEvent
 */
self.addEventListener('install', (event) => {
    console.log(`Install cache ${CACHE_NAME}`);
    let precache = async () => {
        let cache = await caches.open(CACHE_NAME);
        return cache.addAll([
            '/',
            "entities/announcement.js",
            "entities/apple.js",
            "entities/bald.js",
            "entities/bullet.js",
            "entities/cat.js",
            "entities/cybertux.js",
            "entities/city_traffic.js",
            "entities/collectible.js",
            "entities/dialog.js",
            "entities/dragon.js",
            "entities/duck.js",
            "entities/enemy.js",
            "entities/exit.js",
            "entities/explosion_area.js",
            "entities/fadeout.js",
            "entities/fampire.js",
            "entities/flower.js",
            "entities/heart.js",
            "entities/huerotate.js",
            "entities/key.js",
            "entities/lava.js",
            "entities/moving_cloud.js",
            "entities/poison.js",
            "entities/sara_plane.js",
            "entities/sara.js",
            "entities/tatou.js",
            "entities/tengu.js",
            "entities/vulnerable.js",
            "entities/vulture.js",
            "entities/witch.js",
            "favicon.png",
            "favicon512.png",
            "index.css",
            "index.html",
            "manifest.json",
            "maps/cloud/cloud_bg.png",
            "maps/city/city.png",
            "maps/city/city1.tmx",
            "maps/city/city.tsx",
            "maps/city/sara_home.png",
            "maps/city/sara_home.tmx",
            "maps/city/sara_home.tsx",
            "maps/city/cybertux.png",
            "maps/city/cybertux.tmx",
            "maps/city/cybertux.tsx",
            "maps/collectibles/collectibles.licence",
            "maps/collectibles/collectibles.png",
            "maps/collectibles/collectibles.tsx",
            "maps/icons/cat-icon.png",
            "maps/icons/fampire-icon.png",
            "maps/icons/icons.tsx",
            "maps/icons/sara-icon.png",
            "maps/icons/tatou-icon.png",
            "maps/icons/vulture-icon.png",
            "maps/icons/witch-icon.png",
            "maps/mountain/mountain_1.tmx",
            "maps/mountain/mountain_2.tmx",
            "maps/mountain/mountain_3.tmx",
            "maps/mountain/mountain_4.tmx",
            "maps/mountain/mountain_5.tmx",
            "maps/mountain/mountain_bg.tsx",
            "maps/mountain/mountain_intro.tmx",
            "maps/mountain/mountain_outro.tmx",
            "maps/mountain/mountain.png",
            "maps/mountain/mountain.tsx",
            "maps/mountain/sky.png",
            "maps/tower/lava_deep.png",
            "maps/tower/sky.png",
            "maps/tower/tower_1.tmx",
            "maps/tower/tower_bg.tsx",
            "maps/tower/tower_bg0_brick1.png",
            "maps/tower/tower_bg0_brick2.png",
            "maps/tower/tower_bg0_brick3.png",
            "maps/tower/tower_bg0_demon_female.png",
            "maps/tower/tower_bg0_demon_male.png",
            "maps/tower/tower.png",
            "maps/tower/tower.tsx",
            "musics/centurion_of_war/chippy_melodyv2.ogg",
            "musics/centurion_of_war/church_combat.ogg",
            "musics/centurion_of_war/deviation2.ogg",
            "musics/centurion_of_war/guitar_53.ogg",
            "musics/centurion_of_war/helldrake.ogg",
            "musics/centurion_of_war/LICENSE.md",
            "musics/centurion_of_war/overdrive.ogg",
            "musics/centurion_of_war/push_ahead.ogg",
            "musics/centurion_of_war/pushing_yourself2.ogg",
            "musics/centurion_of_war/carnage.ogg",
            "musics/centurion_of_war/no_starsmixered.ogg",
            "opensara.js",
            "playnewton.js",
            "scenes/city_cybertux_level.js",
            "scenes/city_intro_level.js",
            "scenes/city_level.js",
            "scenes/cloud_level.js",
            "scenes/mountain_intro_level.js",
            "scenes/mountain_level.js",
            "scenes/mountain_outro_level.js",
            "scenes/scene.js",
            "scenes/title.js",
            "scenes/tower_level.js",
            "scenes/tutorial_level.js",
            "serviceWorker.js",
            "sounds/attack-fampire-electric.wav",
            "sounds/attack-fampire-fire.wav",
            "sounds/attack-fampire-phire.wav",
            "sounds/attack-vulture.wav",
            "sounds/collect-apple.wav",
            "sounds/collect-exit.wav",
            "sounds/collect-heart.wav",
            "sounds/collect-key.wav",
            "sounds/die-fampire.wav",
            "sounds/explosion.wav",
            "sounds/hurt-enemy.wav",
            "sounds/hurt-sara.wav",
            "sounds/jump-sara.wav",
            "sounds/menu-select.wav",
            "sounds/pause_resume.wav",
            "sounds/poison.wav",
            "sounds/shoot-annie.wav",
            "sounds/shoot-bald.wav",
            "sounds/shoot-cat.wav",
            "sounds/shoot-dragon.wav",
            "sounds/shoot-duck.wav",
            "sounds/shoot-sara.wav",
            "sounds/shoot-tengu.wav",
            "sounds/skip.wav",
            "sounds/tripleshoot-cybertux.wav",
            "sounds/sideshoot-cybertux.wav",
            "sounds/shoot-cybertuxmini.wav",
            "sounds/teleport.wav",
            "sprites/annie.png",
            "sprites/bald.png",
            "sprites/capuchine.png",
            "sprites/cat.png",
            "sprites/duck.png",
            "sprites/fampire.png",
            "sprites/moving_clouds.png",
            "sprites/sara.licence",
            "sprites/sara.png",
            "sprites/seaplane.png",
            "sprites/tatou.png",
            "sprites/tengu.png",
            "sprites/title.png",
            "sprites/tutorial.png",
            "sprites/vulture.png",
            "sprites/witch.png",
            "utils/keyboard_mappings.js",
            "utils/z_order.js",
            ]);
    }
    event.waitUntil(precache());
});

/**
 * @type FetchEvent
 */
self.addEventListener('fetch', (event) => {
    let respond = async () => {
        let cache = await caches.open(CACHE_NAME);
        const cachedResponse = await cache.match(event.request);
        if (cachedResponse) {
            return cachedResponse;
        } else {
            return fetch(event.request);
        }
    }
    event.respondWith(respond());
});

self.addEventListener('activate', function (event) {
    let updateCache = async () => {
        let keys = await caches.keys();
        return Promise.all(keys.map((key) => {
            if (key !== CACHE_NAME) {
                console.log(`Delete cache ${key}`);
                return caches.delete(key);
            }
        }));
    }
    event.waitUntil(updateCache());
});
