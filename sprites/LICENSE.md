# License notes

Some files in this folder are derived work.

## sara.png and seaplane.png

Original author: Mandi Paugh
Edited by: devnewton
License: CC-BY 3.0
Link: https://opengameart.org/content/sara-wizard
